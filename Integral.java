package com.story.framework.core.utils;


import java.math.BigDecimal;

/**
 * @Author: Xiaohei
 * @CreateTime: 2021/8/31 22:57
 */
public class Integral {
    public static void main(String[] args) {
        double total = 333.3;
        double n =10;
        double coefficient = 1;
        double b = getB(total, n, coefficient);
        System.out.println(b);
        double sum = 0;
        double time = 50;
        for (int i = 1; i <= time; i++) {
            double v = 0.3*(i*(n/time) - b) * (i*(n/time) - b);
            System.out.println(v);
            sum += v*(n/time);
        }
        System.out.println("sum:" + sum);
    }

    public static double getB(double total, double n, double coefficient) {
        double ab = Math.pow(n,4.0) - 4*n*(Math.pow(n,3.0)/3-total/coefficient);
        System.out.println(ab);
        double b1 = (Math.pow(n,2.0) + Math.sqrt(ab))/2/n;
        double b2 = (Math.pow(n,2.0) - Math.sqrt(ab))/2/n;
        if (b1>0&&b2>0) {
            return Math.min(b1, b2);
        }
        if (b1 < 0) {
            return b2;
        }
        if (b2<0) {
            return b1;
        }
        return 0;
    }
}
