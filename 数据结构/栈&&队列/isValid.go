package main

func isValid(s string) bool {
	mMap := map[byte]byte{}
	mMap['('] = ')'
	mMap['{'] = '}'
	mMap['['] = ']'
	stack := []byte{}
	stack = append(stack, s[0])
	for i := 1; i < len(s); i++ {
		if len(stack) == 0 {
			stack = append(stack, s[i])
			continue
		}
		if s[i] == mMap[stack[len(stack)-1]] {
			stack = stack[:len(stack)-1]
		} else {
			stack = append(stack, s[i])
		}
	}
	if len(stack) == 0 {
		return true
	} else {
		return false
	}
}

func main() {
	str := "()[]{}"
	valid := isValid(str)
	println(valid)
}
