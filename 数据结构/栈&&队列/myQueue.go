package main

type MyQueue struct {
	inQueue  []int
	outQueue []int
}

func Constructor() MyQueue {
	myQueue := MyQueue{}
	return myQueue
}

func (this *MyQueue) Push(x int) {
	this.inQueue = append(this.inQueue, x)
}

func (this *MyQueue) Pop() int {
	if len(this.outQueue) == 0 {
		this.in2out()
	}
	num := this.outQueue[len(this.outQueue)-1]
	this.outQueue = this.outQueue[:len(this.outQueue)-1]
	return num
}

func (this *MyQueue) in2out() {
	for len(this.inQueue) > 0 {
		this.outQueue = append(this.outQueue, this.inQueue[len(this.inQueue)-1])
		this.inQueue = this.inQueue[:len(this.inQueue)-1]
	}
}

func (this *MyQueue) Peek() int {
	if len(this.outQueue) == 0 {
		this.in2out()
	}
	return this.outQueue[len(this.outQueue)-1]
}

func (this *MyQueue) Empty() bool {
	if len(this.inQueue) == 0 && len(this.outQueue) == 0 {
		return true
	} else {
		return false
	}
}

func main() {
	myQueue := Constructor()
	myQueue.Push(1)
	myQueue.Push(2)
	peek := myQueue.Peek()
	println(peek)
	pop := myQueue.Pop()
	println(pop)
	empty := myQueue.Empty()
	println(empty)
}
