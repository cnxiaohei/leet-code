package main

import "fmt"

func merge(nums1 []int, m int, nums2 []int, n int) {
	if m == 0 {
		for i := 0; i < n; i++ {
			nums1[i] = nums2[i]
		}
		return
	}
	if n == 0 {
		return
	}
	ptr := m + n - 1
	ptr1 := m - 1
	ptr2 := n - 1
	for ptr >= 0 {
		if ptr2 == -1 {
			return
		}
		if ptr1 == -1 {
			nums1[ptr] = nums2[ptr2]
			nums2[ptr2] = 0
			ptr--
			ptr2--
			continue
		}
		//比较两个数组的末位
		if nums1[ptr1] > nums2[ptr2] {
			nums1[ptr], nums1[ptr1] = nums1[ptr1], 0
			ptr1--
		} else {
			nums1[ptr], nums2[ptr2] = nums2[ptr2], 0
			ptr2--
		}
		ptr--
	}
	if ptr1 == 0 && ptr2 != 0 {
		for i := 0; i < ptr2; i++ {
			nums1[i] = nums2[i]
		}
		return
	}
	if ptr1 == 0 && ptr2 == 0 {
		if nums1[ptr1] > nums2[ptr2] {
			nums1[1] = nums1[ptr1]
			nums1[0] = nums2[ptr2]
		} else {
			nums1[1] = nums2[ptr2]
			nums1[0] = nums1[ptr1]
		}
	}
}

func main() {
	nums1 := []int{1, 2, 3, 0, 0, 0}
	nums2 := []int{2, 5, 6}
	merge(nums1, 3, nums2, 3)
	for i := 0; i < len(nums1); i++ {
		fmt.Println(nums1[i])
	}
}
