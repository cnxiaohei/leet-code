package main

func twoSum(nums []int, target int) []int {
	var nMap = make(map[int]int)
	for i := 0; i < len(nums); i++ {
		index, ok := nMap[target-nums[i]]
		if ok {
			result := []int{i, index}
			return result
		}
		nMap[nums[i]] = i
	}
	result := []int{}
	return result
}

func main() {

}
