package main

import "fmt"

func maxSubArray(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}
	pre := nums[0]
	max := nums[0]
	curr := nums[0]
	for i := 1; i < len(nums); i++ {
		pre = curr
		if pre < 0 {
			curr = nums[i]
		} else {
			curr = pre + nums[i]
		}
		if curr > max {
			max = curr
		}
	}
	return max
}

func main() {
	ii := []int{5, 4, -1, 7, 8}
	max := maxSubArray(ii)
	fmt.Println(max)
}
