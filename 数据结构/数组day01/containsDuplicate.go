package main

import "fmt"

func containsDuplicate(nums []int) bool {
	nMap := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		_, ok := nMap[nums[i]]
		if ok {
			return true
		} else {
			nMap[nums[i]] = 1
		}
	}
	return false
}

func main() {
	ii := []int{1}
	duplicate := containsDuplicate(ii)
	fmt.Println(duplicate)
}
