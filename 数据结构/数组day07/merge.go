package main

func merge(intervals [][]int) [][]int {
	if len(intervals) == 0 {
		return nil
	}
	//排序
	for i := 0; i < len(intervals)-1; i++ {
		for j := 1; j < len(intervals)-i; j++ {
			if intervals[j][0] < intervals[j-1][0] {
				intervals[j], intervals[j-1] = intervals[j-1], intervals[j]
			}
		}
	}
	slow := 0
	fast := slow + 1
	temp := []int{0, 0}
	ans := [][]int{}

	return intervals
}

func main() {
	intervals := [][]int{{1, 4}, {4, 5}, {3, 6}}
	i := merge(intervals)
	print(i)
}
