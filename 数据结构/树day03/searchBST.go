package main

func searchBST(root *TreeNode, val int) *TreeNode {
	if root == nil {
		return nil
	}
	temp := root
	for temp != nil {
		if temp.Val == val {
			return temp
		}
		if temp.Val > val {
			temp = temp.Left
		} else {
			temp = temp.Right
		}
	}
	return nil
}

func main() {

}
