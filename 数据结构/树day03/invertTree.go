package main

func invertTree(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
	invert(root)
	return root
}

func invert(root *TreeNode) {
	if root == nil {
		return
	}
	root.Right, root.Left = root.Left, root.Right
	invert(root.Left)
	invert(root.Right)
}

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {

}
