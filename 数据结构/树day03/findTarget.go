package main

func findTarget(root *TreeNode, k int) bool {
	if root == nil {
		return false
	}
	queue := []*TreeNode{}
	m := map[int]int{}
	queue = append(queue, root)
	for i := 0; i < len(queue); i++ {
		_, ok := m[k-queue[i].Val]
		if ok {
			return true
		} else {
			m[queue[i].Val] = 1
		}
		if queue[i].Left != nil {
			queue = append(queue, queue[i].Left)
		}
		if queue[i].Right != nil {
			queue = append(queue, queue[i].Right)
		}
	}
	return false
}

func main() {

}
