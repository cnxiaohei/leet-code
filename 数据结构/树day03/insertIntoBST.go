package main

func insertIntoBST(root *TreeNode, val int) *TreeNode {
	if root == nil {
		return &TreeNode{Val: val}
	}
	temp := root
	for temp != nil {
		if temp.Val == val {
			return root
		}
		if temp.Val > val {
			if temp.Left != nil {
				temp = temp.Left
			} else {
				temp.Left = &TreeNode{Val: val}
				return root
			}
		} else {
			if temp.Right != nil {
				temp = temp.Right
			} else {
				temp.Right = &TreeNode{Val: val}
				return root
			}
		}
	}
	return root
}

func main() {

}
