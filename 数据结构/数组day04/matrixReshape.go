package main

func matrixReshape(mat [][]int, r int, c int) [][]int {
	if len(mat)*len(mat[0]) != r*c {
		return mat
	}
	rm := make([][]int, r)
	var cm []int
	count := 0
	hPtr := 0
	wPtr := 0
	for i := 0; i < r; i++ {
		cm = make([]int, c)
		for j := 0; j < c; j++ {
			if count >= len(mat[0]) {
				count = 0
				hPtr++
				wPtr = 0
			}
			cm[j%c] = mat[hPtr][wPtr]
			count++
			wPtr++
		}
		rm[i] = cm
	}
	return rm
}

func main() {
	i1 := []int{1, 2}
	i2 := []int{3, 4}
	ii := [][]int{i1, i2}
	reshape := matrixReshape(ii, 1, 4)
	for i := 0; i < len(reshape); i++ {
		for j := 0; j < len(reshape[0]); j++ {
			println(reshape[i][j])
		}
	}
}
