package main

//杨辉三角
func generate(numRows int) [][]int {
	h := make([][]int, numRows)
	for i := 0; i < numRows; i++ {
		w := make([]int, i+1)
		if i == 0 {
			w[i] = 1
			h[i] = w
			continue
		}
		for j := 0; j < i+1; j++ {
			if j == 0 || j == i {
				w[j] = 1
				continue
			}
			w[j] = h[i-1][j-1] + h[i-1][j]
		}
		h[i] = w
	}
	return h
}

func main() {
	i := generate(5)
	println(i)
}
