package main

func intersect(nums1 []int, nums2 []int) []int {
	map1 := make(map[int]int)
	map2 := make(map[int]int)
	length1 := len(nums1)
	length2 := len(nums2)
	if length1 > length2 {
		for i := 0; i < length2; i++ {
			n, ok := map1[nums2[i]]
			if ok {
				map1[nums2[i]] = n + 1
			} else {
				map1[nums2[i]] = 1
			}
		}
		for i := 0; i < length1; i++ {
			n, ok := map1[nums1[i]]
			if ok {
				m, ok := map2[nums1[i]]
				if ok {
					if m < n {
						map2[nums1[i]] = m + 1
					}
				} else {
					map2[nums1[i]] = 1
				}
			}
		}
	} else {
		for i := 0; i < length1; i++ {
			n, ok := map1[nums1[i]]
			if ok {
				map1[nums1[i]] = n + 1
			} else {
				map1[nums1[i]] = 1
			}
		}
		for i := 0; i < length2; i++ {
			n, ok := map1[nums2[i]]
			if ok {
				m, ok := map2[nums2[i]]
				if ok {
					if m < n {
						map2[nums2[i]] = m + 1
					}
				} else {
					map2[nums2[i]] = 1
				}
			}
		}
	}
	result := make([]int, 0)
	for k, v := range map2 {
		for i := 0; i < v; i++ {
			result = append(result, k)
		}
	}
	return result
}

func main() {
	ii := []int{4, 9, 5}
	jj := []int{9, 4, 9, 8, 4}
	kk := intersect(ii, jj)
	for i := 0; i < len(kk); i++ {
		println(kk[i])
	}
}
