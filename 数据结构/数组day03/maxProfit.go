package main

func maxProfit(prices []int) int {
	minCost := prices[0]
	max := 0
	for i := 1; i < len(prices); i++ {
		num := prices[i] - minCost
		if max < num {
			max = num
		} else if prices[i] < minCost {
			minCost = prices[i]
		}
	}
	return max
}

func main() {
	ii := []int{2, 1, 4}
	profit := maxProfit(ii)
	println(profit)
}
