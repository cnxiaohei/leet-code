package main

func majorityElement(nums []int) int {
	if len(nums) < 3 {
		return nums[0]
	}
	eMap := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		num, ok := eMap[nums[i]]
		if ok {
			i2 := num + 1
			if i2 > len(nums)/2 {
				return nums[i]
			}
			eMap[nums[i]] = i2
		} else {
			eMap[nums[i]] = 1
		}

	}
	return 0
}

func main() {

}
