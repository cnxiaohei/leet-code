package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func preorderTraversal(root *TreeNode) []int {
	if root == nil {
		return nil
	}
	list := []int{}
	list = preorder(root, list)
	return list
}

func preorder(node *TreeNode, list []int) []int {
	if node == nil {
		return list
	}
	list = append(list, node.Val)
	list = preorder(node.Left, list)
	list = preorder(node.Right, list)
	return list
}

func main() {
	node3 := TreeNode{Val: 3}
	node2 := TreeNode{Val: 2, Left: &node3}
	node1 := TreeNode{Val: 1, Right: &node2}
	traversal := preorderTraversal(&node1)
	for i := 0; i < len(traversal); i++ {
		println(traversal[i])
	}
}
