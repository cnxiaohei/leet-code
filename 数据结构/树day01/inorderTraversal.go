package main

func inorderTraversal(root *TreeNode) []int {
	if root == nil {
		return []int{}
	}
	var inorder func(root *TreeNode, list []int) []int
	list := []int{}
	inorder = func(root *TreeNode, list []int) []int {
		if root == nil {
			return list
		}
		list = inorder(root.Left, list)
		list = append(list, root.Val)
		list = inorder(root.Right, list)
		return list
	}
	list = inorder(root, list)
	return list
}

func main() {
	node3 := TreeNode{Val: 3}
	node2 := TreeNode{Val: 2, Left: &node3}
	node1 := TreeNode{Val: 1, Right: &node2}
	traversal := inorderTraversal(&node1)
	for i := 0; i < len(traversal); i++ {
		println(traversal[i])
	}
}
