package main

func postorderTraversal(root *TreeNode) []int {
	if root == nil {
		return []int{}
	}
	list := []int{}
	var postorder func(root *TreeNode, list []int) []int
	postorder = func(root *TreeNode, list []int) []int {
		if root == nil {
			return list
		}
		list = postorder(root.Left, list)
		list = postorder(root.Right, list)
		list = append(list, root.Val)
		return list
	}
	list = postorder(root, list)
	return list
}

func main() {

}
