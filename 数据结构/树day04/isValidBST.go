package main

import "math"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isValidBST(root *TreeNode) bool {
	//递归
	var dg func(*TreeNode, int, int) bool
	dg = func(node *TreeNode, left, right int) bool {
		if node == nil {
			return true
		}
		if node.Val >= right || node.Val <= left {
			return false
		}
		return dg(node.Left, left, node.Val) && dg(node.Right, node.Val, right)
	}
	return dg(root, math.MinInt64, math.MaxInt64)
}

func main() {

}
