package main

import (
	"sort"
	"strings"
)

func isAnagram(s string, t string) bool {
	split1 := strings.Split(s, "")
	split2 := strings.Split(t, "")
	sort.Strings(split1)
	sort.Strings(split2)
	join1 := strings.Join(split1, "")
	join2 := strings.Join(split2, "")
	if join1 == join2 {
		return true
	}
	return false
}

func main() {
	split1 := strings.Split("abc", "")
	join := strings.Join(split1, "")
	println(join)
}
