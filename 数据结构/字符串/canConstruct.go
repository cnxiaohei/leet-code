package main

import "strings"

func canConstruct(ransomNote string, magazine string) bool {
	for _, ch := range ransomNote {
		if !strings.Contains(magazine, string(ch)) {
			return false
		} else {
			index := strings.LastIndex(magazine, string(ch))
			magazine = magazine[:index] + magazine[index+1:]
		}
	}
	return true
}

func main() {
	ransomNote := "aa"
	magazine := "ab"
	construct := canConstruct(ransomNote, magazine)
	println(construct)
}
