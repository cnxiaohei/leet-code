package main

func removeElements(head *ListNode, val int) *ListNode {
	temp := head
	prev := head
	for temp != nil {
		if temp.Val == val {
			if head == prev {
				head = head.Next
				prev = head
				temp = head
				continue
			}
			prev.Next = temp.Next
			temp = temp.Next
			continue
		}
		prev = temp
		temp = temp.Next
	}
	return head
}

func main() {

}
