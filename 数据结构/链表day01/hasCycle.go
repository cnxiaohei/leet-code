package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func hasCycle(head *ListNode) bool {
	nodeMap := map[*ListNode]int{}
	temp := head
	has := false
	index := 0
	for temp != nil {
		_, ok := nodeMap[temp]
		if ok {
			has = true
			break
		}
		nodeMap[temp] = index
		temp = temp.Next
		index++
	}
	return has
}

func main() {

}
