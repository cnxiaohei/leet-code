package main

func deleteDuplicates(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	prev := head
	temp := head
	for prev.Next != nil {
		if prev.Val == temp.Val {
			prev.Next = temp.Next
			temp = temp.Next
			continue
		}
		prev = temp
		temp = temp.Next
	}
	return head
}

func main() {

}
