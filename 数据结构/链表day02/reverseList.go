package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	if head.Next == nil {
		return head
	}
	temp := head
	l1 := []*ListNode{}
	for temp != nil {
		l1 = append(l1, temp)
		temp = temp.Next
	}
	result := &ListNode{}
	tempR := result
	for i := len(l1) - 1; i >= 0; i-- {
		l1[i].Next = nil
		tempR.Next = l1[i]
		tempR = tempR.Next
	}
	return result.Next
}

func main() {
	l5 := &ListNode{Val: 5}
	l4 := &ListNode{Val: 4, Next: l5}
	l3 := &ListNode{Val: 3, Next: l4}
	l2 := &ListNode{Val: 2, Next: l3}
	l1 := &ListNode{Val: 1, Next: l2}
	node := reverseList(l1)
	println(node)
}
