package main

import (
	"fmt"
)

func isValidSudoku(board [][]byte) bool {
	result := true
	ch := make(chan bool)
	//行校验
	for i := 0; i < 9; i++ {
		go func(i int) {
			nn := make([]int, 9)
			for j := 0; j < 9; j++ {
				num := board[i][j]
				if num == '.' {
					continue
				}
				nn[num-49]--
				if nn[num-49] < -1 {
					ch <- false
					return
				}
			}
			ch <- true
		}(i)
	}
	//列校验
	for i := 0; i < 9; i++ {
		go func(i int) {
			nn := make([]int, 9)
			for j := 0; j < 9; j++ {
				num := board[j][i]
				if num == '.' {
					continue
				}
				nn[num-49]--
				if nn[num-49] < -1 {
					ch <- false
					return
				}
			}
			ch <- true
		}(i)
	}
	//单元校验
	for i := 0; i < 9; i++ {
		go func(i int) {
			nn := make([]int, 9)
			h := 0
			ww := 0
			for j := 0; j < 9; j++ {
				h = i/3*3 + j/3
				ww = i%3*3 + j%3
				num := board[h][ww]
				if num == '.' {
					continue
				}
				nn[num-49]--
				if nn[num-49] < -1 {
					ch <- false
					return
				}
			}
			ch <- true
		}(i)
	}

	for i := 0; i < 27; i++ {
		println(i)
		if result = <-ch; result {
			continue
		} else {
			return result
		}
	}
	//defer close(ch)
	return result
}

var (
	i1  = []byte{'5', '3', '.', '.', '7', '.', '.', '.', '.'}
	i2  = []byte{'6', '.', '.', '1', '9', '5', '.', '.', '.'}
	i3  = []byte{'.', '9', '8', '.', '.', '.', '.', '6', '.'}
	i4  = []byte{'8', '.', '.', '.', '6', '.', '.', '.', '3'}
	i5  = []byte{'4', '.', '.', '8', '.', '3', '.', '.', '1'}
	i6  = []byte{'7', '.', '.', '.', '2', '.', '.', '.', '6'}
	i7  = []byte{'.', '6', '.', '.', '.', '.', '2', '8', '.'}
	i8  = []byte{'.', '.', '.', '4', '1', '9', '.', '.', '5'}
	i9  = []byte{'.', '.', '.', '.', '8', '.', '.', '7', '9'}
	src = [][]byte{i1, i2, i3, i4, i5, i6, i7, i8, i9}
)

func main() {
	ii := []byte{'1', '2'}
	for _, b := range ii {
		println(b)
	}
	sudoku := isValidSudoku(src)
	fmt.Println(sudoku)
}
