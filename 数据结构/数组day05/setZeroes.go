package main

func setZeroes(matrix [][]int) {
	n := len(matrix)
	m := len(matrix[0])
	//l1行,l2列
	l1 := make([]int, n)
	l2 := make([]int, m)
	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			if matrix[i][j] == 0 {
				l1[i] = 1
				l2[j] = 1
			}
		}
	}
	for i := 0; i < n; i++ {
		if l1[i] == 1 {
			matrix[i] = make([]int, m)
		}
	}
	for i := 0; i < m; i++ {
		if l2[i] == 1 {
			for j := 0; j < n; j++ {
				matrix[j][i] = 0
			}
		}
	}
}

func main() {

}
