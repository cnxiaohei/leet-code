package com.story.framework.core.utils.Hot100.java;


import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Xiaohei
 * @CreateTime: 2022/2/14 22:02
 */
public class TwoSum {

    public static void main(String[] args) {
        int[] nums = {3, 2, 4};
        int[] ints = twoSum(nums, 6);
        System.out.println(ints.toString());
    }

    /**
     * 执行用时：1 ms, 在所有 Java 提交中击败了99.32%的用户
     * 内存消耗：41.5 MB, 在所有 Java 提交中击败了8.93%的用户
     * @param nums
     * @param target
     * @return
     */
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> sums = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (i == 0) {
                sums.put(num, i);
                continue;
            }
            if (sums.containsKey(target - num)) {
                return new int[]{i, sums.get(target - num)};
            }else {
                sums.put(num, i);
            }
        }
        return null;
    }
}
