package com.story.framework.core.utils.Hot100.java;


/**
 * @Author: Xiaohei
 * @CreateTime: 2022/2/14 22:21
 */
public class ListNode {

    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
