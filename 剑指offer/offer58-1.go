package main

import "strings"

func reverseWords(s string) string {
	s = strings.Trim(s, "")
	split := strings.Split(s, " ")
	str := []string{}
	for i := 0; i < len(split); i++ {
		if split[i] != "" {
			str = append(str, split[i])
		}
	}
	for i := 0; i < len(str)/2; i++ {
		str[i], str[len(str)-1-i] = str[len(str)-1-i], str[i]
	}
	join := strings.Join(str, " ")
	return join
}

func main() {
	words := reverseWords("  ")
	println(words)
}
