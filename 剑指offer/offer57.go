package main

func twoSum(nums []int, target int) []int {
	m := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		_, ok := m[target-nums[i]]
		if ok {
			return []int{nums[i], target - nums[i]}
		} else {
			m[nums[i]] = i
		}
	}
	return nil
}

func main() {

}
