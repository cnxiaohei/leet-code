package main

import "math"

func minArray(numbers []int) int {
	if numbers == nil {
		return 0
	}
	min := math.MaxInt32
	for i := 0; i < len(numbers); i++ {
		if min > numbers[i] {
			min = numbers[i]
		}
	}
	return min
}

func main() {

}
