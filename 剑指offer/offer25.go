package main

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	head := &ListNode{Val: 0}
	temp := head
	for true {
		if l1 == nil {
			temp.Next = l2
			break
		}
		if l2 == nil {
			temp.Next = l1
			break
		}
		if l1.Val < l2.Val {
			temp.Next = l1
			l1 = l1.Next
		} else {
			temp.Next = l2
			l2 = l2.Next
		}
		temp = temp.Next
	}
	return head.Next
}

func main() {

}
