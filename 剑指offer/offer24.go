package main

func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return head
	}
	prev := head
	curr := head.Next
	next := head.Next
	prev.Next = nil
	for curr != nil {
		next = curr.Next
		curr.Next = prev
		prev = curr
		curr = next
	}
	return prev
}

func main() {

}
