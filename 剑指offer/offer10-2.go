package main

func numWays(n int) int {
	if n <= 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	const mod int = 1e9 + 7
	p := 0
	q := 1
	r := 1
	for i := 1; i < n; i++ {
		p = q
		q = r
		r = (p + q) % mod
	}
	return r
}

func main() {
	ways := numWays(43)
	println(ways)
}
