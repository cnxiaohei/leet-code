package main

func maxValue(grid [][]int) int {

	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[0]); j++ {
			v1 := 0
			v2 := 0
			if j-1 >= 0 {
				v1 = grid[i][j-1]
			}
			if i-1 >= 0 {
				v2 = grid[i-1][j]
			}
			grid[i][j] = max(v1, v2) + grid[i][j]
		}
	}
	return grid[len(grid)-1][len(grid[0])-1]
}

func main() {
	m := [][]int{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}}
	value := maxValue(m)
	println(value)
}
