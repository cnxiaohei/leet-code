package main

func firstUniqChar(s string) byte {
	if s == "" {
		return ' '
	}
	timeMap := make(map[byte]int)
	for i := 0; i < len(s); i++ {
		_, ok := timeMap[s[i]]
		if ok {
			timeMap[s[i]]++
		} else {
			timeMap[s[i]] = 1
		}
	}
	for i := 0; i < len(s); i++ {
		time := timeMap[s[i]]
		if time == 1 {
			return s[i]
		}
	}
	return ' '
}

func main() {

}
