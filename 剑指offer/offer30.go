package main

import (
	"math"
)

type MinStack struct {
	stack []int
	min   int
}

/** initialize your data structure here. */
func Constructor1() MinStack {
	return MinStack{}
}

func (this *MinStack) Push(x int) {
	this.stack = append(this.stack, x)
}

func (this *MinStack) setMin() {
	m := math.MaxInt32
	for i := 0; i < len(this.stack); i++ {
		m = min(m, this.stack[i])
	}
	this.min = m
}

func (this *MinStack) Pop() {
	if len(this.stack) > 0 {
		this.stack = this.stack[:len(this.stack)-1]
	}
}

func (this *MinStack) Top() int {
	if len(this.stack) > 0 {
		return this.stack[len(this.stack)-1]
	}
	return 0
}

func (this *MinStack) Min() int {
	this.setMin()
	return this.min
}

func min(x, y int) int {
	if x < y {
		return x
	} else {
		return y
	}
}

func main() {

}
