package main

func fib(n int) int {
	if n == 0 {
		return 0
	}
	if n == 1 {
		return 1
	}
	const mod int = 1e9 + 7
	p := 0
	q := 0
	r := 1
	for i := 1; i < n; i++ {
		p = q
		q = r
		r = (p + q) % mod
	}
	return r
}

func main() {

}
