package main

func levelOrder3(root *TreeNode) [][]int {
	result := [][]int{}
	if root == nil {
		return result
	}
	currQueue := []*TreeNode{}
	nextQueue := []*TreeNode{}
	currQueue = append(currQueue, root)
	index := 0
	temp := []int{}
	for index < len(currQueue) {
		temp = append(temp, currQueue[index].Val)
		if currQueue[index].Left != nil {
			nextQueue = append(nextQueue, currQueue[index].Left)
		}
		if currQueue[index].Right != nil {
			nextQueue = append(nextQueue, currQueue[index].Right)
		}
		index++
		if index == len(currQueue) {
			if len(result)%2 == 1 {
				left := 0
				right := len(temp) - 1
				for left < right {
					temp[left], temp[right] = temp[right], temp[left]
					left++
					right--
				}
			}
			result = append(result, temp)
			temp = []int{}
			currQueue = nextQueue
			nextQueue = []*TreeNode{}
			index = 0
		}
	}
	return result
}

func main() {

}
