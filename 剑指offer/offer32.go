package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

//广度优先遍历
func levelOrder(root *TreeNode) []int {
	if root == nil {
		return []int{}
	}
	queue := []*TreeNode{}
	result := []int{}
	queue = append(queue, root)
	for i := 0; i < len(queue); i++ {
		result = append(result, queue[i].Val)
		if queue[i].Left != nil {
			queue = append(queue, queue[i].Left)
		}
		if queue[i].Right != nil {
			queue = append(queue, queue[i].Right)
		}
	}
	return result
}

func main() {

}
