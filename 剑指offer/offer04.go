package main

func findNumberIn2DArray(matrix [][]int, target int) bool {
	if len(matrix) == 0 || len(matrix[0]) == 0 {
		return false
	}
	m := len(matrix)
	n := len(matrix[0])
	for i := 0; i < m; i++ {
		if target >= matrix[i][0] || target <= matrix[i][n-1] {
			for j := 0; j < n; j++ {
				if target == matrix[i][j] {
					return true
				}
			}
		}
	}
	return false
}

func main() {

}
