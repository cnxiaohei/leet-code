package main

import "math"

func maxSubArray(nums []int) int {
	maxSum := math.MinInt32
	prev := 0
	curr := 0
	for i := 0; i < len(nums); i++ {
		if prev < 0 {
			curr = nums[i]
		} else {
			curr = prev + nums[i]
		}
		maxSum = max(maxSum, curr)
		prev = curr
	}
	return maxSum
}

func max(i, j int) int {
	if i > j {
		return i
	} else {
		return j
	}
}

func main() {

}
