package main

func reverseLeftWords(s string, n int) string {
	bytes := make([]byte, len(s))
	for i := n; i < len(s); i++ {
		bytes[i-n] = s[i]
	}
	for i := n; i > 0; i-- {
		bytes[len(s)-i] = s[n-i]
	}
	return string(bytes)
}

func main() {
	leftWords := reverseLeftWords("abcdefg", 2)
	println(leftWords)
}
