package main

func exchange(nums []int) []int {
	n := len(nums)
	nn := make([]int, n)
	single := 0
	dou := n - 1
	for i := 0; i < n; i++ {
		if nums[i]%2 == 0 {
			nn[dou] = nums[i]
			dou--
		} else {
			nn[single] = nums[i]
			single++
		}
	}
	return nn
}

func main() {

}
