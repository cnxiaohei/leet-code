package main

import "sort"

func findRepeatNumber(nums []int) int {
	sort.Ints(nums)
	for i := 1; i < len(nums); i++ {
		if nums[i-1] == nums[i] {
			return nums[i]
		}
	}
	return 0
}

func main() {

}
