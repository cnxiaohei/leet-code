package main

func lengthOfLongestSubstring(s string) int {
	if s == "" {
		return 0
	}
	maxx := 1
	curr := 0
	subMap := make(map[byte]int)
	slow := 0
	fast := slow

	for fast < len(s) {
		_, ok := subMap[s[fast]]
		if ok {
			maxx = max(maxx, curr)
			curr = 0
			subMap = make(map[byte]int)
			slow++
			fast = slow
		} else {
			subMap[s[fast]] = 1
			curr++
			fast++
		}
	}
	maxx = max(maxx, curr)
	return maxx
}

func main() {
	num := lengthOfLongestSubstring("dvdf")
	println(num)
}
