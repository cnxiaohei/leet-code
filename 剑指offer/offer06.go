package main

func reversePrint(head *ListNode) []int {
	if head == nil {
		return []int{}
	}
	if head.Next == nil {
		return []int{head.Val}
	}
	ints := reversePrint(head.Next)
	ints = append(ints, head.Val)
	return ints
}

func main() {

}
