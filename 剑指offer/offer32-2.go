package main

func levelOrder2(root *TreeNode) [][]int {
	result := [][]int{}
	if root == nil {
		return result
	}
	currQueue := []*TreeNode{}
	nextQueue := []*TreeNode{}
	currQueue = append(currQueue, root)
	index := 0
	temp := []int{}
	for index < len(currQueue) {
		temp = append(temp, currQueue[index].Val)
		if currQueue[index].Left != nil {
			nextQueue = append(nextQueue, currQueue[index].Left)
		}
		if currQueue[index].Right != nil {
			nextQueue = append(nextQueue, currQueue[index].Right)
		}
		index++
		if index == len(currQueue) {
			result = append(result, temp)
			temp = []int{}
			currQueue = nextQueue
			nextQueue = []*TreeNode{}
			index = 0
		}
	}
	return result
}

func main() {

}
