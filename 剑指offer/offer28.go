package main

func isSymmetric(root *TreeNode) bool {
	if root == nil {
		return true
	}
	return aaa(root, root)
}

func aaa(root, mirror *TreeNode) bool {
	if root.Val != mirror.Val {
		return false
	}
	return aaa(root.Left, mirror.Right) && aaa(root.Right, mirror.Left)
}

func main() {

}
