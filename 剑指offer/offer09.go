package main

import "container/list"

type CQueue struct {
	stack1, stack2 *list.List
}

func Constructor() CQueue {
	return CQueue{
		stack1: list.New(),
		stack2: list.New(),
	}
}

func (this *CQueue) AppendTail(value int) {
	this.stack1.PushBack(value)
}

func (this *CQueue) DeleteHead() int {
	if this.stack2.Len() == 0 {
		for this.stack1.Len() > 0 {
			this.stack2.PushBack(this.stack1.Remove(this.stack1.Back()))
		}
	}
	if this.stack2.Len() == 0 {
		return -1
	} else {
		back := this.stack2.Back()
		this.stack2.Remove(back)
		return back.Value.(int)
	}
}

func main() {
	queue := Constructor()
	queue.AppendTail(31)
	queue.AppendTail(21)
	queue.AppendTail(25)
	queue.AppendTail(1)
	num := queue.DeleteHead()
	println(num)
}
