package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func getKthFromEnd(head *ListNode, k int) *ListNode {

	temp := head
	length := 0
	for temp != nil {
		temp = temp.Next
		length++
	}
	for i := 0; i < length-k; i++ {
		head = head.Next
	}
	return head
}

func main() {

}
