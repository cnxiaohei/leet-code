package main

func deleteNode(head *ListNode, val int) *ListNode {
	//临时节点
	temp := head
	//前节点
	prev := head
	//当下一个节点不为空时循环
	for temp != nil {
		if temp.Val == val {
			if temp == head {
				return temp.Next
			}
			prev.Next = temp.Next
			break
		} else {
			prev = temp
			temp = temp.Next
		}
	}
	return head
}

func main() {
	n1 := &ListNode{Val: 4}
	n2 := &ListNode{Val: 5}
	n3 := &ListNode{Val: 1}
	n4 := &ListNode{Val: 9}
	n1.Next = n2
	n2.Next = n3
	n3.Next = n4
	node := deleteNode(n1, 9)
	println(node)
}
