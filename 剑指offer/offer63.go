package main

func maxProfit(prices []int) int {
	buy := 0
	sell := len(prices) - 1
	profit := 0
	for buy < len(prices)-1 {
		if sell == buy {
			buy++
			sell = len(prices) - 1
			continue
		}
		if prices[sell]-prices[buy] > profit {
			profit = prices[sell] - prices[buy]
		} else {
			sell--
		}
	}
	return profit
}

func main() {
	stock := []int{7, 1, 5, 3, 6, 4}
	profit := maxProfit(stock)
	println(profit)
}
