package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

func main() {
	cmdExec := `dir`
	cmd := exec.Command("cmd", "/C", cmdExec)
	out, err := cmd.Output()
	cmd.Run()
	if err != nil {
		fmt.Println(err)
		return
	}
	cmd.Wait()
	fmt.Println(string(out))
}

func rename(originalName, newName string) {

}

func getCurrentAbPathByExecutable() string {
	exePath, err := os.Executable()
	if err != nil {
		log.Fatal(err)
	}
	res, _ := filepath.EvalSymlinks(filepath.Dir(exePath))
	return res
}
