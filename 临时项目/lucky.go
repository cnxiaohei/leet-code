package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	var num int
	fmt.Println("输入抽奖人数：")
	fmt.Scanln(&num)
	arr := make([]string, num)
	for i := 0; i < num; i++ {
		var name string
		fmt.Printf("输入第%d名抽奖选手：\n", i+1)
		fmt.Scanln(&name)
		arr[i] = name
	}
	s := lucky(arr)
	fmt.Println("最终的获奖选手是!!!")
	fmt.Println(arr[s])
}

func lucky(arr []string) int {
	rand.Seed(time.Now().Unix())
	random := rand.Intn(len(arr))
	return random
}
