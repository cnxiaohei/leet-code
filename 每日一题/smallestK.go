package main

//输入n个数,取前k小的数
func smallestK(arr []int, k int) []int {
	mini := []int{}
	for i := 0; i < len(arr); i++ {
		sortInsert(arr[i], mini)
		if len(mini) > k {
			mini = mini[:k]
		}
	}
	return mini
}

func sortInsert(x int, l []int) {
	l = append(l, x)
	for i := len(l) - 1; i > 0; i-- {
		if l[i] >= l[i-1] {
			break
		} else {
			l[i], l[i-1] = l[i-1], l[i]
		}
	}
}

func main() {

}
