package main

import "fmt"

func firstBadVersion(n int) int {
	left := 1
	right := n
	for left != right {
		if right-left == 1 {
			if (isBadVersion(right) == true) && (isBadVersion(left) == false) {
				return right
			}
		}
		if isBadVersion((right + left) / 2) {
			right = (right + left) / 2
		} else {
			left = (right + left) / 2
		}
	}
	return left
}

func isBadVersion(n int) bool {
	if n >= 10 {
		return true
	} else {
		return false
	}
}

func main() {
	version := firstBadVersion(10)
	fmt.Println(version)
}
