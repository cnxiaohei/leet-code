package main

func combine(n int, k int) (ans [][]int) {
	//计算数组容量
	list := []int{}
	var dfs func(int)
	dfs = func(curr int) {
		if len(list)+(n-curr+1) < k {
			return
		}
		//list符合条件后加入结果集
		if len(list) == k {
			temp := make([]int, k)
			copy(temp, list)
			ans = append(ans, temp)
			return
		}
		//加入现在的选中值
		list = append(list, curr)
		dfs(curr + 1)
		//回溯
		list = list[:len(list)-1]
		dfs(curr + 1)
	}
	dfs(1)
	return
}

func main() {
	ans := combine(5, 2)
	println(ans)
}
