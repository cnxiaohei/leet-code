package main

import "strings"

func letterCasePermutation(s string) (ans []string) {
	if s == "" {
		return []string{""}
	}
	ans = append(ans, "")
	//使用标记
	for _, char := range s {

		if (char >= 'A' && char <= 'Z') || (char >= 'a' && char <= 'z') {
			//字符
			n := len(ans)
			for j := 0; j < n; j++ {
				//新增一个位置给大/小写
				ans = append(ans, ans[j])
				ans[j] = ans[j] + strings.ToLower(string(char))
				ans[j+n] = ans[j+n] + strings.ToUpper(string(char))
			}
		} else {
			//数字
			for k := 0; k < len(ans); k++ {
				ans[k] = ans[k] + string(char)
			}
		}
	}
	return ans
}

func modify(s string, i int) string {
	char := s[i]
	var data []byte = []byte(s)
	//大写字母
	if char >= 'A' && char <= 'Z' {
		char += 32
	}
	if char >= 'a' && char <= 'z' {
		char -= 32
	}
	data[i] = char
	return string(data)
}

func main() {
	ppp := letterCasePermutation("a1b2")
	println(ppp)
}
