package main

func sortedSquares(nums []int) []int {
	left := 0
	right := len(nums) - 1
	newNums := make([]int, len(nums))
	for i := len(nums) - 1; i >= 0; i-- {
		n1 := nums[right] * nums[right]
		n2 := nums[left] * nums[left]
		if n1 >= n2 {
			newNums[i] = n1
			right--
		} else {
			newNums[i] = n2
			left++
		}
	}
	return newNums
}

func main() {

}
