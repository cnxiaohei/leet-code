package main

import "fmt"

func rotate(nums []int, k int) {
	if k > len(nums) {
		k = k % len(nums)
	}
	nMap := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		if i+k > len(nums)-1 {
			nums[(i+k)%len(nums)] = nums[i]
		} else {
			nMap[i+k] = nums[i]
		}
		i2, ok := nMap[i]
		if ok {
			nums[i] = i2
		}
	}
}

func main() {
	ii := []int{1, 2, 3, 4, 5, 6}
	rotate(ii, 11)
	for i := 0; i < len(ii); i++ {
		fmt.Println(ii[i])
	}
}
