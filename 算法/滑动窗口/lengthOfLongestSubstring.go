package main

func lengthOfLongestSubstring(s string) int {
	fast := 0
	slow := 0
	count := 0
	strMap := make(map[byte]int)
	for slow < len(s) {
		u := s[slow]
		_, ok := strMap[u]
		if ok {
			strMap = make(map[byte]int)
			fast++
			slow = fast
			if slow-fast > count {
				count = slow - fast
			}
		} else {
			strMap[u] = slow
			slow++
			if slow-fast > count {
				count = slow - fast
			}
		}
	}
	return count
}

func main() {
	i := lengthOfLongestSubstring(" ")
	println(i)
}
