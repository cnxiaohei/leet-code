package main

import (
	"sort"
	"strconv"
	"time"
)

//滑动窗口
func checkInclusion(s1 string, s2 string) bool {
	l1 := len(s1)
	l2 := len(s2)
	if l1 > l2 {
		return false
	}
	var ch1, ch2 [26]int32
	//构造窗口
	for i, c := range s1 {
		ch1[c-'a']++
		//在s2中构造
		ch2[s2[i]-'a']++
	}
	if ch1 == ch2 {
		return true
	}
	for i := 0; i < l2-l1; i++ {
		ch2[s2[i]-'a']--
		ch2[s2[i+l1]-'a']++
		if ch1 == ch2 {
			return true
		}
	}
	return false
}

func main() {
	s1 := "cbb"
	b1 := make([]string, 3)
	for i := 0; i < 3; i++ {
		b1[i] = string(s1[i])
	}
	sort.Strings(b1)
	for _, s := range b1 {
		print(s)
	}
	println()
	a1 := "aaa"
	a2 := "aabaacsaa"
	before := time.Now()
	inclusion := checkInclusion(a1, a2)
	after := time.Now()
	println("任务用时:" + strconv.FormatInt((after.UnixNano()-before.UnixNano())/1e6, 10) + "ms")
	println(inclusion)
}
