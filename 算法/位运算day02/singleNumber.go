package main

func singleNumber(nums []int) (ans int) {
	ans = 1
	for i := 0; i < len(nums); i++ {
		ans &= nums[i]
	}
	return
}

func main() {
	i := 2 ^ 2
	println(i)
}
