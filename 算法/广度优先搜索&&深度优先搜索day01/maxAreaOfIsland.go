package main

func maxAreaOfIsland(grid [][]int) int {
	maxArea := 0
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[0]); j++ {
			if grid[i][j] == 1 {
				area := dfs2(grid, j, i)
				if area > maxArea {
					maxArea = area
				}
			}
		}
	}
	return maxArea
}

func dfs2(grid [][]int, x, y int) int {
	area := 1
	grid[y][x] = 0
	for i := 0; i < 4; i++ {
		mx, my := x+dx[i], y+dy[i]
		if mx >= 0 && my >= 0 && mx < len(grid) && my < len(grid[0]) && 1 == grid[my][mx] {
			area += dfs2(grid, mx, my)
		}
	}
	return area
}

func main() {

}
