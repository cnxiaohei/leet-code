package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func middleNode(head *ListNode) *ListNode {
	var m = make(map[int]*ListNode)
	temp := head
	count := 1
	for temp != nil {
		m[count] = temp
		count++
		temp = temp.Next
	}
	num := count/2 + count%2*1
	node := m[num]
	return node
}

func main() {
	n1 := ListNode{Val: 10}
	n2 := ListNode{Val: 5, Next: &n1}
	n3 := ListNode{Val: 1, Next: &n2}
	node := middleNode(&n3)
	fmt.Println(node)
}
