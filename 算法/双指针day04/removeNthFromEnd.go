package main

//双指针yyds
func removeNthFromEnd(head *ListNode, n int) *ListNode {
	if head.Next == nil {
		return nil
	}
	ptr1 := head
	ptr2 := head
	for i := 0; i < n; i++ {
		ptr2 = ptr2.Next
	}
	if ptr2 == nil {
		return head.Next
	}
	for ptr2.Next != nil {
		ptr1 = ptr1.Next
		ptr2 = ptr2.Next
	}
	if n == 1 {
		ptr1.Next = nil
	} else {
		ptr1.Next = ptr1.Next.Next
	}
	return head
}

func main() {
	n1 := &ListNode{Val: 5}
	n2 := &ListNode{Val: 4, Next: n1}
	n3 := &ListNode{Val: 3, Next: n2}
	//n4 := &ListNode{Val: 2, Next: n3}
	//n5 := &ListNode{Val: 1, Next: n4}
	node := removeNthFromEnd(n3, 3)
	for node != nil {
		println(node.Val)
		node = node.Next
	}

}
