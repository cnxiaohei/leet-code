package main

import "fmt"

//思路就是双指针,快慢指针
//慢指针指向第一个0,快指针寻找第一个非零
func moveZeroes(nums []int) {

	fast := 0
	slow := 0
	for slow < len(nums) && fast < len(nums) {
		if nums[slow] != 0 {
			slow++
			fast = slow
			continue
		}
		if nums[fast] != 0 {
			nums[slow], nums[fast] = nums[fast], nums[slow]
			slow++
			//fast = slow
		} else {
			fast++
		}
	}
}

func main() {
	ii := []int{0, 1, 0, 3, 12}
	moveZeroes(ii)
	for i := 0; i < len(ii); i++ {
		fmt.Println(ii[i])
	}
}
