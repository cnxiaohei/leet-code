package main

import "fmt"

//源数据为升序数组
//主策略:设计左右指针,当左右值的和大于target则降低right指针位置,由于升序数组的原因,和必变小,当左右值的和小于target则提升left指针位置,
//同理和会变大,这样可以慢慢逼近target
func twoSum(numbers []int, target int) []int {
	left := 0
	right := len(numbers) - 1
	for numbers[left]+numbers[right] != target {
		if numbers[left]+numbers[right] > target {
			right--
		} else {
			left++
		}
	}
	result := []int{left + 1, right + 1}
	return result
}

func main() {
	ii := []int{5, 25, 75}
	sum := twoSum(ii, 100)
	for i := 0; i < len(sum); i++ {
		fmt.Println(sum[i])
	}
}
