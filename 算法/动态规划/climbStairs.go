package main

//动态规划,n阶=n-1阶 + n-2阶
func climbStairs(n int) int {
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	l, p, m := 0, 0, 1
	for i := 0; i < n; i++ {
		l, p = p, m
		m = l + p
	}
	return m
}

func main() {
	num := climbStairs(44)
	println(num)
}
