package main

func rob(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}
	if len(nums) == 2 {
		return max(nums[0], nums[1])
	}
	dp1 := nums[0]
	dp2 := max(nums[0], nums[1])
	dp3 := max(dp1+nums[2], dp2)
	for i := 3; i < len(nums); i++ {
		dp1, dp2 = dp2, dp3
		dp3 = max(dp1+nums[i], dp2)
	}
	return dp3
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func main() {

}
