package main

func isPowerOfTwo(n int) bool {
	if n == 1 {
		return true
	}
	for i := 0; i < n; i++ {
		if 2<<i == n {
			return true
		}
		if 2<<i > n {
			return false
		}
	}
	return false
}

func main() {
	powerOfTwo := isPowerOfTwo(1)
	println(powerOfTwo)
}
