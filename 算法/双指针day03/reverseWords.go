package main

import "strings"

func reverseWords(s string) string {
	words := strings.Split(s, " ")
	length := len(words)
	sss := ""
	for i := 0; i < length; i++ {
		c := []byte(words[i])
		left := 0
		right := len(c) - 1
		for left < right {
			c[left], c[right] = c[right], c[left]
			left++
			right--
		}
		ss := string(c)
		if i != length-1 {
			ss += " "
		}
		sss += ss
	}
	return sss
}

func main() {

}
